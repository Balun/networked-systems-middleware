package hr.fer.zemris.pus.lab1.task3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class VideoCountMap extends
        Mapper<LongWritable, Text, Text, Text> {

    private static final int THRESHOLD = 5;

    @Override
    public void map(LongWritable key, Text value, Context context)
            throws
            IOException,
            InterruptedException {

        String[] parts = (value.toString()).split(" ");
        String id = parts[0];
        int percent = Integer.parseInt(parts[1]);

        if (percent > THRESHOLD) {
            context.write(new Text(id),
                          new Text(Integer.toString(percent)));
        }

    }
}
