package hr.fer.zemris.pus.lab1.task3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

public class VideoCountReduce extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws
            IOException,
            InterruptedException {

        int count = 0;
        for (Text v : values) {
            count += 1;
        }
        context.write(new Text(key),
                      new Text(Integer.toString(count)));
    }
}
