package hr.fer.zemris.pus.lab1.task4;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

public class MatrixMultiply {

    public static void main(String[] args) throws
            IOException,
            ClassNotFoundException,
            InterruptedException {
        if (args.length != 5) {
            System.err.println("Need to specifiy output and input " +
                                       "directories, as well as matrix dim.");
            System.exit(-1);
        }

        Configuration conf = new Configuration();
        // M is an m-by-n matrix; N is an n-by-p matrix.
        conf.set("m",
                 args[2]);
        conf.set("n",
                 args[3]);
        conf.set("p",
                 args[4]);
        @SuppressWarnings("deprecation")
        Job job = new Job(conf,
                          "MatrixMultiply");
        job.setJarByClass(MatrixMultiply.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(MatrixMultiplyMap.class);
        job.setReducerClass(MatrixMultiplyReduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job,
                                     new Path(args[0]));
        FileOutputFormat.setOutputPath(job,
                                       new Path(args[1]));

        job.waitForCompletion(true);
    }
}
