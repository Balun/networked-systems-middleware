package hr.fer.zemris.pus.lab1.task3;

import hr.fer.zemris.pus.lab1.task4.MatrixMultiply;
import hr.fer.zemris.pus.lab1.task4.MatrixMultiplyMap;
import hr.fer.zemris.pus.lab1.task4.MatrixMultiplyReduce;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;


public class VideoCount {

    public static void main(String[] args) throws
            IOException,
            ClassNotFoundException,
            InterruptedException {
        if (args.length != 2) {
            System.err.println("Usage: VideoCount <input path> <output path " +
                                       ">");
            System.exit(-1);
        }

        Configuration conf = new Configuration();

        @SuppressWarnings("deprecation")
        Job job = new Job(conf,
                          "Video COunt");
        job.setJarByClass(VideoCount.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setSortComparatorClass(IntComparator.class);

        job.setMapperClass(VideoCountMap.class);
        job.setReducerClass(VideoCountReduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job,
                                     new Path(args[0]));
        FileOutputFormat.setOutputPath(job,
                                       new Path(args[1]));

        job.waitForCompletion(true);

    }

    public static class IntComparator extends WritableComparator {

        public IntComparator() {
            super(Text.class,
                  true);
        }

        @Override
        public int compare(WritableComparable a, WritableComparable b) {
            Text v1 = (Text) a;
            Text v2 = (Text) b;

            return -1 * Integer.compare(Integer.parseInt(v1.toString()),
                                   Integer.parseInt(v2.toString()));
        }
    }

}
